﻿<!-- Дополнительное задание:
1. Создайте базовый класс продукта по аналогии с рассмотренным в примерах.
2. Создайте три любых типа продукта (класса), отличных от приведенных в лекции в разных категориях.
3. Все продукты, кроме одного, имеют 10-процентную скидку и их цена должна выводиться с ней.
4. Один тип продукта имеет скидку только в том случае, если его вес больше 10 килограмм.
5. Доставка на все продукты = 250 рублей, но если на продукт была скидка, то 300 рублей.
6. Используйте примеси, интерфейсы или абстрактные классы в решении задачи.
 -->

<?php

    //создаем базовый класс
    abstract class Product 
    {
        public $name;
        public $category;
        protected $cost;
        public $weight;
        protected $discount; 
        protected $price;
        protected $delivery;                  

        public function __construct($name, $category, $cost, $weight)
        {
            $this->name = $name;
            $this->category = $category;
            $this->cost = $cost;
            $this->weight = $weight;
            echo "Вы покупаете товар:  $this->name. ";
        }        
    } 

    //создаем трэйт получения цены для товаров без скидки
     trait GetPrice 
    {           
        public function getPrice() 
        {               
            $this->price = $this->cost;
            echo "Цена товара {$this->price} руб. На этот товар скидок нет. ";
        }        
    }    

    //создаем трэйт получения стоимости доставки
    trait GetPriceDelivery 
    {           
        public function getPriceDelivery() 
        {
            if ($this->discount == 0) {
            $this->delivery = 250;
            } else {
            $this->delivery = 300;
            }
            echo "Цена доставки {$this->delivery} руб. ";
        }        
    }

    //создаем трэйт получения цены для товаров со скидкой
    trait GetDiscountedPrice 
    {           
        public function getDiscountedPrice() 
        {   
            $this->discount = 10;
            $this->price = round(($this->cost - $this->cost*$this->discount/100), 2);
            echo "Цена товара со скидкой {$this->price} руб.";
        }        
    }    

    //создаем трэйт получения цены для товаров со скидкой в зависимости от веса
    trait GetPriceOnWeight 
    {           
        public function GetPriceOnWeight() 
        {
            if ($this->weight > 10) {
            $this->discount = 10;
            $this->price = round(($this->cost - $this->cost*$this->discount/100), 2);
             echo "Цена товара со скидкой  {$this->price} руб. ";
            } else {
            $this->discount = 0;
             echo "Цена товара {$this->cost} руб. Возьмите больше 10 кг и получите скидку. ";
            }           
        }        
    }

    // На этот товар скидки нет

    class HygienicGoods extends Product
    {
        use GetPrice, GetPriceDelivery; 
    }

    $toothbrush = new HygienicGoods("Зубная щетка", "Гигиенические товары", 600, 2);
    echo $toothbrush->getPrice();
    echo $toothbrush->getPriceDelivery(). "<br>";

    // Этот товар имеет 10% скидку при весе более 10 кг

    class Hardware extends Product
    {         
        use GetPriceOnWeight, GetPriceDelivery;                
    }

    $nails = new Hardware("Гвозди", "скобяные изделия", 500, 20);    
    echo $nails->GetPriceOnWeight();
    echo $nails->getPriceDelivery() . "<br>";
    $screws = new Hardware("Шурупы", "скобяные изделия", 400, 10);
    echo $screws->GetPriceOnWeight();
    echo $screws->getPriceDelivery(). "<br>";

    // 'этот товар имеет скидку всегда'

    class ProductsForChildren extends Product
    {
        use GetDiscountedPrice, GetPriceDelivery; 
    }

    $diapers = new ProductsForChildren("Подгузники", "Товары для детей", 600, 2);
    echo $diapers->getDiscountedPrice();
    echo $diapers->getPriceDelivery(). "<br>";

    
   
   
 ?>