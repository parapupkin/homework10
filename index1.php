﻿<!-- 1.1. Наследование - заимствование всех свойств и метедов родителя без копирования кода;
     1.2. Полиморфизм - возможность различного поведения наследников, в том числе и при переопределении методов и свойств;
     2. Отличия абстрактных классов от интерфейсов:
        2.1. САМОЕ ГЛАВНОЕ - абстрактный класс описывает сущность объекта, а интерфейс - поведение объекта в изоляции от самого объекта. Т.е. метод плавать может быть и у подводной лодки и у рыбы,
        но они не могут быть одним классом, но могут содержать один интерфейс - "плавать";
        2.2 Абстрактный класс может содержать обычные методы, а интерфейс - нет; :);
        2.3 Абстрактный класс может содержать свойства, а интерфейс - нет;
        2.4 Имплементировать можно сколько угодно интерфейсов, а наследовать класс - только от одного родителя;
        2.5 Абстрактный класс может имплементировать интерфейсы
        2.6 Абстрактный класс может содержать методы и свойства protected и privet, а интерфейс - нет;
     Абстрактные классы нужно использовать тогда, когда есть несколько ОДИНАКОВЫХ сущностей, обладающих одним или несколькими методами с разной реализацией, а интерфейсы нужно использовать для того, чтобы описать РАЗНЫЕ сущности, имеющие одинаковые методы, разные в реализации -->
<?php
    
    // суперклассом для классов утка, машина, товар, телевизор и шариковая ручка может быть класс объект

    abstract class Object 
    {
        public $name; 
        public $weight;
        public $price;

        public function __construct($name, $weight, $price)  
        {
           $this->name = $name;
           $this->weight = $weight;
           $this->price = $price; 
           echo "Этот объект создан на основе расширяемого класса и с использованием интерфейса <br>";                 
        } 

        public function weighing() 
        {
            echo "Как по весу? ";
            if ($this->weight > 50) {
                echo "Этот объект тяжело поднять";
            } else {
                echo "Поднять можно, но спину сорвешь :)";
            };   
        } 
    }

    interface Using 
    {
        public function getNewCopy();
        public function humanUsе();
    } 

    class ProductClass extends Object implements Using    
    {         
        public $category;
        public function pricing() 
        {
           
            if ($this->price > 10000) {
                echo "Ты что, Сдурел?.. такое дорогое покупать!";
            } else {
                echo "Покупай, нормально по цене";
            };    
        }
        public function getNewCopy() 
        {
            echo "Чтобы получить новый товар нужно его купить :)";
        }

        public function humanUsе() 
        {
            echo "Человек будет использовать этот товар по своему усмотрению :)";
        }        

    }

      $coat = new ProductClass("coat", 5, 15000);
      $coat->category = "clothes";        
      // echo $coat->weighing() . ".<br>";
      // echo $coat->humanUsе() . ".<br>";

      class MachineClass extends Object implements Using     
  {     
    public $material;
    public $color;
    public $sound;

    public function getSoundOfWork() 
    {
        echo "Я работаю со звуком: " . $this->sound;    
    }
    public function getNewCopy() 
    {
        echo "Новую машину производят на заводе :)";
    }

    public function humanUsе() 
    {
        echo "Меня используют в личных целях :)";
    }        
  }

  $washer = new MachineClass("indesit", 20, 1000);  
  $washer->material = "plastic";
  $washer->color = "white";
  $washer->sound = "ж-ж-ж";
  // echo "Я машина " . $washer->name . ". "; 
  // echo $washer->getSoundOfWork() . ".<br>";


  $car = new MachineClass("Запорожец", 1500, 2000);  
  $car->material = "metal";
  $car->color = "black";
  $car->sound = "бр-бр-бр";
  // echo "Я машина " . $car->name . ". "; 
  // echo $car->getSoundOfWork() . ".<br>"; 
  // echo $car->humanUsе() . ".<br>";

  class TVClass extends Object implements Using  
  {     
    public $diagonalSize;
    public $screenResolution;
    public $energyConsumption;

    public function getPowerConsumption() 
    {
        echo "При работе 120 часов в месяц я израсходую " . $this->energyConsumption * 120/1000 . " кВт";    
    }
    public function getNewCopy() 
    {
        echo "Телик тоже производят на заводе :)";
    }

    public function humanUsе() 
    {
        echo "Нужен для просмотра фильмов и олимпиады 2018 :)";
    }        
  }

  $Lg = new TVClass("Lg", 5, 1000);  
  $Lg->diagonalSize = 40;
  $Lg->screenResolution = "4k";
  $Lg->energyConsumption = "50 Вт";  
  // echo $Lg->getPowerConsumption() . ".<br>";
  // echo $Lg->humanUsе() . ".<br>";  

  $Samsung = new TVClass("Samsung", 6, 1200);  
  $Samsung->diagonalSize = 40;
  $Samsung->screenResolution = "5k";
  $Samsung->energyConsumption = "60 Вт";  
  // echo $Samsung->getPowerConsumption() . ".<br>";

  class BallpenClass extends Object implements Using 
  {     
    public $color;     

    public function getLetter() 
    {
        echo "Цвет моих чернил " . $this->color;    
    }
    public function getNewCopy() 
    {
        echo "Ручки делают китайцы :)";
    }

    public function humanUsе() 
    {
        echo "Нужна для писанины :)";
    }        
  }

  $pilot = new BallpenClass("pilot", 0.02, 5);    
  $pilot->color = "синий";   
  // echo $pilot->getLetter() . ".<br>";

  $bic = new BallpenClass("bic", 0.03, 6);   
  $bic->color = "черный";   
  // echo $bic->getLetter() . ".<br>";

  class DuckClass extends Object implements Using  
  { 
    public $habitat;
    public $age;   

    public function getMeat() 
    {
        echo "Я утка весом " . $this->weight . " кг и из меня получится " . $this->weight * 0.5 . " кг мяса";    
    }
    public function getNewCopy() 
    {
        echo "Уток делают другие утки :)";
    }

    public function humanUsе() 
    {
        echo "Нужны для мяса и перьев :)";
    }        
  }

  $donald = new DuckClass("donald", 5, 100);  
  $donald->habitat = "farm";
  $donald->age = "2 года";  
  // echo $donald->getMeat() . ".<br>";

  $scrooge = new DuckClass("scrooge", 6, 120);  
  $scrooge->habitat = "wild nature";
  $scrooge->age = "3 года";  
  // echo $scrooge->getMeat() . ".<br>";

  ?>